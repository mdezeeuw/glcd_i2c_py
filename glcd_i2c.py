#!/usr/bin/env python
import smbus
import time

class GLCD:

   cmd_clear = 1
   cmd_set_cursor = 2
   cmd_set_backlight = 3
   cmd_set_font = 4
   cmd_image = 5
   cmd_show_logo = 6

   bus_nr = 1
   address = None
 
   write_delay = 0.01

   def detectBusNr(self):

      try:
         import RPi.GPIO

         # for RPI version 1, use "bus = smbus.SMBus(0)"
         rev = RPi.GPIO.RPI_REVISION
         if rev != 1:
            self.bus_nr = 1 
         else:
            self.bus_nr = 0
      except:
         pass
      
   def __init__(self, bus_nr=0, address=9):
      self.bus_nr = bus_nr

      # Try to autodetect bus_nr. This will fail when no sudo access (I think)
      if bus_nr is None:
          self.detectBusNr()

      self.address = address

      self.bus = smbus.SMBus(bus_nr)

   def writeBlock(self, cmd, data):
     self.bus.write_i2c_block_data(self.address, cmd, data)
     time.sleep(self.write_delay)

   def write(self, *data):
     # print "Writing {}".format(data)

     for c in data:
       
       if isinstance(c, str):
         pass

       elif c == None:
         pass

       else:
         self.bus.write_byte(self.address, c)
	 time.sleep(self.write_delay)

   def clear(self):
      self.write(self.cmd_clear)

   def setCursor(self, x, y):
     self.writeBlock(self.cmd_set_cursor, [x, y])

   def showLogo(self):
     self.write(self.cmd_show_logo)

   def setBacklight(self, value):
     if value >= 0 and value < 256:
        self.writeBlock(self.cmd_set_backlight, [value])

   def setFont(self, font):
     if font == 'fixed':
        self.writeBlock(self.cmd_set_font, [1])
     elif font == 'proportional':
        self.writeBlock(self.cmd_set_font, [2])

   def printTxt(self, txt):
     data = [ord(x) for x in list(txt)]
     self.writeBlock(data[0], data[1:])

if __name__ == "__main__":
   lcd = GLCD(None, 9)
   #lcd.init(None, 9)
   lcd.clear()
   lcd.setCursor(1, 1)
   lcd.setFont('proportional')
   lcd.printTxt("Integratie Token")
   lcd.setFont('fixed')
   lcd.setCursor(1, 40)
   lcd.printTxt(local_ip_address)
   lcd.setBacklight(155)
   time.sleep(0.2)

